/**
 * @providesModule Toggle
 */
import tabbable from 'tabbable'
import Component from 'Lib/Component'
import keycode from 'Lib/keycode'
import { guid } from 'Lib/helper'

export default class Accordion extends Component {
  constructor (element, target, initialState) {
    super(element)
    this.target = target
    this.initialState = initialState
    this.guid = guid()
    this.state = {
      open: false
    }

    // Mount this component
    this.mount()
  }

  toggleState () {
    return this.setState({
      open: !this.state.open
    })
  }

  // Action handler
  actionHandler (e) {
    // Prevent default action of element
    e.preventDefault()

    return e.type === 'click' ? this.toggleState() : e.type === 'keypress' ? (e.which === keycode.space || e.which === keycode.enter ? this.toggleState() : false) : false
  }

  // Mount component
  mount () {
    // Add click listener to element
    this.element.addEventListener('click', (e) => { this.actionHandler(e) })

    // Add keypress listener to element
    this.element.addEventListener('keypress', (e) => { this.actionHandler(e) })

    // Remove focus when clicked with mouse
    this.element.addEventListener('mouseup', () => {
      setTimeout(() => {
        this.element.blur()
      }, 150)
    })

    // Add tabindex if not tabbable
    if (tabbable(this.element).length === 0) {
      this.element.setAttribute('tabindex', '0')
    }

    // Setup button
    this.element.setAttribute('aria-controls', this.guid)
    this.element.setAttribute('role', 'button')

    // Setup content
    this.target.id = this.guid

    return this.setState()
  }

  render () {
    if (this.state.open) {
      // Set element on
      this.element.classList.add('is-open')
      this.element.setAttribute('aria-expanded', 'true')

      // Set target on
      this.target.classList.add('is-open')
      this.target.inert = false
      this.target.setAttribute('aria-hidden', 'false')
    } else {
      // Set element off
      this.element.classList.remove('is-open')
      this.element.setAttribute('aria-expanded', 'false')

      // Set target off
      this.target.classList.remove('is-open')
      this.target.inert = true
      this.target.setAttribute('aria-hidden', 'true')
    }
    return true
  }
}
