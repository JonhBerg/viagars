/**
 * @providesModule tre3
 */
import { Map } from 'immutable'

export default class Component {
  constructor (element) {
    this.element = element
    this.state = {}
  }

  setState (object = {}) {
    const previousState = Map(this.state)
    const stateUpdates = Map(object)
    const newState = previousState.merge(stateUpdates)
    this.state = newState.toObject()

    return this.render()
  }

  render () {
    return true
  }
}
