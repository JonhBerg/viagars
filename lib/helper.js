export function combinedHeight (element) {
  // Measure all content elements and assign their height to a css variable in the style attribute of the html.
  let childrenCombinedHeight = 0;
  [...element.children].forEach((child) => {
    // Get margins
    const style = child.currentStyle || window.getComputedStyle(child)
    const verticalMargins = parseInt(style.marginTop, 10) + parseInt(style.marginBottom, 10)

    // Calculate child total height
    childrenCombinedHeight = childrenCombinedHeight + child.offsetHeight + verticalMargins
  })
  return childrenCombinedHeight
}

export function debounce (callback, wait, context = this) {
  let timeout = null
  let callbackArgs = null

  const later = () => callback.apply(context, callbackArgs)

  return function callbackFunction () {
    callbackArgs = arguments
    clearTimeout(timeout)
    timeout = setTimeout(later, wait)
  }
}

export function isColor (string) {
  return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(string)
}

export function safeTagsReplace (str) {
  const tagsToReplace = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
  }

  function replaceTag (tag) {
    return tagsToReplace[tag] || tag
  }

  return str.replace(/[&<>]/g, replaceTag)
}

export function hasClass (elem, cls) {
  var str = ' ' + elem.className + ' '
  var testCls = ' ' + cls + ' '
  return (str.indexOf(testCls) !== -1)
}

export function guid () {
  function s4 () {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1)
  }
  return `${s4()}${s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`
}
