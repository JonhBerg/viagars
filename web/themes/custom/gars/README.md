# √iaGars Drupal 8 Theme

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

## Theme development

See documentation in the main [README](../../../../../README.md#frontendtheme-development) file

## √iaGars theme file structure

Theme folder/file structure

```
├─ *.js             # Entrypoint JavaScript files are placed into the root of theme
│
├─ assets/
│  ├─ component/    # Directory for Sass/JavaScript partials
│  ├─ element/      # Directory for Sass/JavaScript partials
│  ├─ index.js      # Entrypoint JavaScript file.
│  └─ style.scss    # Entrypoint CSS file, includes all the imports for other css files
│
├─ inc/             # Directory containing preprocess functions for theme.
|
├─ templates/       # Drupal Twig templates
│ 
|
├─ gars.info.yml    # Theme configuration file
├─ gars.layouts.yml # DisplaySuite layout configuration file
|
└─ README.md        # This README file
```
