import 'babel-polyfill'
import 'wicg-inert'
import 'svgxuse'
// import objectFitImages from 'object-fit-images'
// import cssCustomPropertySupport from 'Lib/cssCustomPropertySupport'
// import Navigation from './component/navigation/nav'
// import toggle from './component/toggle/toggle'
// import Tabs from './component/tabs/Tabs'
// import responsiveTables from './component/table/responsive-tables'

// Require svg files
require.context('./svg/', true, /\.(svg)$/)

// Require theme style
require('./style.scss')

document.addEventListener('DOMContentLoaded', () => {

})

// Add class if touch device
document.addEventListener('touchstart', function addtouchclass (e) {
  document.documentElement.classList.add('is-touch')
  document.removeEventListener('touchstart', addtouchclass, false)
}, false)

// Add class if css custom properties are not supported
if (!cssCustomPropertySupport()) {
  document.documentElement.classList.add('is-not-modern')
}
