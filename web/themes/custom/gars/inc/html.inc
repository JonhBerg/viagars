<?php

/**
 * @file
 * Provides html processing functionality.
 */

/**
 * Implements of template_preprocess_html().
 */
function gars_preprocess_html(&$variables) {
  // Do magic when we have a node object.
  $node = \Drupal::routeMatch()->getParameter('node');
  if (is_object($node)) {
    // Set node type as theme-class.
    $variables['attributes']['class'][] = 'theme--' . $node->getType();
    if (!$node->isPublished()) {
      $variables['attributes']['class'][] = 'theme--unpublished';
    }
  }
}
