<?php

/**
 * @file
 * Provides region processing functionality.
 */

/**
 * Implements template_preprocess_HOOK() for region.html.twig;.
 */
function gars_preprocess_region(&$variables) {
  $variables['attributes']['class'][] = 'region';
  $variables['attributes']['class'][] = 'region--' . $variables['elements']['#region'];
  $variables['attributes']['class'][] = $variables['elements']['#region'];

  $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $variables['language'] = $language;
}
