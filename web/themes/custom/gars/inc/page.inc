<?php

/**
 * @file
 * Provides page processing functionality.
 */

/**
 * Implements template_preprocess_page()
 */
function gars_preprocess_page(&$variables) {
  $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $variables['language'] = $language;
  // drupal_set_message('Status message', 'status');
  // drupal_set_message('I am a warning message', 'warning');
  // drupal_set_message('This is error message', 'error');
}
