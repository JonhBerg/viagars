<?php

/**
 * @file
 * Includes gars theme field preprocess functionality.
 */

/**
 * Helper function to associate fields with icons.
 */
function _gars_field_icons() {
  $field_icons = [
    'field_blog' => 'blog',
    'field_project_area_of_focus' => 'focus',
    'field_project_web_pages' => 'web',
    'field_project_type' => 'type',
    'field_project_funding_duration' => 'duration',
    'field_project_social_facebook' => 'facebook',
    'field_project_social_instagram' => 'instagram',
    'field_project_social_twitter' => 'twitter',
    'field_project_social_blog' => 'blog',
    'field_group_area_of_focus' => 'focus',
    'field_group_website' => 'web',
    'field_group_type' => 'type',
    'field_group_social_facebook' => 'facebook',
    'field_group_social_instagram' => 'instagram',
    'field_group_social_twitter' => 'twitter',
    'field_group_social_blog' => 'blog',
    'field_degree_social_facebook' => 'facebook',
    'field_degree_social_instagram' => 'instagram',
    'field_degree_social_twitter' => 'twitter',
    'field_user_facebook' => 'facebook',
    'field_user_linkedin' => 'linkedin',
    'field_user_twitter' => 'twitter',
    'field_work_email' => 'email',
    'field_phone_number' => 'phone',
    'field_campus' => 'location',
    'field_orcid' => 'user',
    'field_email' => 'email',
    'field_address' => 'location',
    'field_staffed_hours' => 'time',
    'field_more_information' => 'none',
    'field_facebook' => 'facebook',
    'field_twitter' => 'twitter',
    'field_linkedin' => 'linkedin',
    'field_instagram' => 'instagram',
  ];

  return $field_icons;
}

/**
 * Helper function to render fields as toggle.
 */
function _gars_field_toggle() {
  $field_icons = [
    'field_group_members_outside_tuni',
    'field_group_research_focus_goals',
    'field_project_phases',
    'field_project_role_in_project',
    'field_project_coordinating_orgs',
    'field_project_people',
    'field_project_partners',
    'field_project_cooperators',
    'field_project_funding',
    'field_top_achievements',
    'field_funding',
    'field_mission_statement',
    'field_positions_of_trust',
    'field_selected_publications',
    'field_cv',
    'field_projects',
  ];

  return $field_icons;
}

/**
 * Implements template_preprocess_field().
 */
function gars_preprocess_field(&$variables) {
  $drupal_fields = ['body', 'description'];

  // Set fields classes.
  if (isset($variables['field_name'])) {
    $field_name = $variables['field_name'];
    $field_css_name = 'field__' . $field_name;

    if (in_array($variables['field_name'], $drupal_fields)) {
      $field_name_clean = $field_name;
    }
    else {
      $field_name_stack = explode('_', trim($field_name));
      array_shift($field_name_stack);
      $field_name_clean = implode('-', $field_name_stack);
    }

    $bundle_name = str_replace('_', '-', $variables['element']['#bundle']);
    $variables['attributes']['class'][] = 'field';
    $variables['attributes']['class'][] = $field_css_name;
    $variables['title_attributes']['class'][] = 'field-title';
    $variables['title_attributes']['class'][] = $field_css_name . '__heading';
    $variables['title_attributes']['class'][] = $bundle_name . '__heading';
    $variables['title_attributes']['class'][] = 'label__' . $variables['label_display'];

    // Set bundle name as class
    if ($bundle_name == $field_name_clean) {
      $variables['attributes']['class'][] = $bundle_name;
    }
    else {
      $variables['attributes']['class'][] = $bundle_name . '__' . $field_name_clean;
    }

    // Add counting classes for fields with multiple items.
    foreach (array_keys($variables['items']) as $delta) {
      $variables['items'][$delta]['attributes']->setAttribute('class', 'field-value ' . $bundle_name . '__item' . ' ' . $bundle_name . '__item--' . $field_name_clean);
    }

    // Set icon for field.
    $icon_fields = _gars_field_icons();
    $variables['icon_id'] = '';
    if (array_key_exists($field_name, $icon_fields)) {
      $variables['icon_id'] = $icon_fields[$field_name];
    }

    // Set permanent title for studyinfo.fi link
    if ($field_name == 'field_degree_link_in_study_info') {
      $variables['items'][0]['content']['#title'] = t('Apply via Studyinfo.fi portal');
    }

    // Set permanent title for scholarship link
    if ($field_name == 'field_degree_link_to_scholarship') {
      $variables['items'][0]['content']['#title'] = t('Scholarships available');
    }

    // Set permanent title for register link
    if ($field_name == 'field_nondegree_register') {
      $variables['items'][0]['content']['#title'] = t('Register');
    }

    // Set "Content owner" label
    if (
      $field_name == 'field_content_owner' ||
      $field_name == 'field_university'
      ) {
      // Get numeric value for field
      $node = $variables['element']['#object'];
      $field_value = $node->get($field_name)->getValue();
      $field_value = (int) $field_value[0]['value'];

      // Set icon according to the field value
      switch ($field_value) {
        case 1:
          $variables['icon_id'] = 'university-label';
          break;

        case 2:
          $variables['icon_id'] = 'uoas-label';
          break;

        case 0:
        default:
          $variables['icon_id'] = 'universities-label';
          $variables['attributes']['class'][] = 'content-label--joint';
          break;
      }
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function gars_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  $field = $variables['element'];
  $field_name = $field['#field_name'];
  $bundle = $field['#bundle'];

  // Set icon field template.
  $icon_fields = _gars_field_icons();
  if (array_key_exists($field_name, $icon_fields)) {
    $suggestions[] = 'field__icon';
    $suggestions[] = 'field__icon__' . $field_name;
    $suggestions[] = 'field__icon__' . $field_name . '__' . $bundle;
  }

  // Set toggle field template.
  $toggle_fields = _gars_field_toggle();
  if (in_array($field_name, $toggle_fields)) {
    $suggestions[] = 'field__toggle';
    $suggestions[] = 'field__toggle__' . $field_name;
    $suggestions[] = 'field__toggle__' . $field_name . '__' . $bundle;
  }

  // Use template with an animated link and arrow
  if (
    $field_name == 'field_degree_link_in_study_info' ||
    $field_name == 'field_nondegree_register' ||
    ($field_name == 'field_links' && in_array($bundle, ['liftup_hero_1', 'liftup_links']))
  ) {
    $suggestions[] = 'field__field_url_link';
  }

  if ($bundle == 'user' && $field_name == 'field_university') {
    $suggestions[] = 'field__field_content_owner';
  }
}
