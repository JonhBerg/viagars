<?php

/**
 * @file
 * Provides head processing functionality.
 */

/**
 * Implements hook_page_attachments_alter().
 */
function gars_page_attachments_alter(&$variables) {

  //   $path_to_theme = '/' . drupal_get_path('theme', 'gars') . '/';
  //   $path_to_images = '/sites/default/files/assets/images/';

  //   // Normal favicon.
  //   foreach ($variables['#attached']['html_head_link'] as $key => $headitem) {
  //     if ($headitem[0]['rel'] == 'shortcut icon') {
  //       $variables['#attached']['html_head_link'][$key][0]['href'] = $path_to_theme . 'favicon.ico';
  //     }
  //   }

  //   // Apple touch icon.
  //   $apple = [
  //     '#tag' => 'link',
  //     '#attributes' => [
  //       'href' => $path_to_images . 'apple-touch-icon.png',
  //       'rel' => 'apple-touch-icon',
  //       'sizes' => '180x180',
  //     ],
  //   ];
  //   $variables['#attached']['html_head'][] = [$apple, 'apple-touch-icon'];

  //   // Larger favicons.
  //   $favicon_sizes = [16, 32];
  //   foreach ($favicon_sizes as $size) {
  //     $favicon = [
  //       '#tag' => 'link',
  //       '#attributes' => [
  //         'href' => $path_to_images . 'favicon-' . $size . 'x' . $size . '.png',
  //         'rel' => 'icon',
  //         'sizes' => $size . 'x' . $size,
  //       ],
  //     ];
  //     $variables['#attached']['html_head'][] = [$favicon, 'favicon-' . $size];
  //   }

  //   // Manifest.
  //   $manifest = [
  //     '#tag' => 'link',
  //     '#attributes' => [
  //       'href' => $path_to_theme . 'site.webmanifest',
  //       'rel' => 'manifest',
  //     ],
  //   ];
  //   $variables['#attached']['html_head'][] = [$manifest, 'manifest'];

  //   // Safari pinned tab.
  //   $safari = [
  //     '#tag' => 'link',
  //     '#attributes' => [
  //       'href' => $path_to_theme . 'safari-pinned-tab.svg',
  //       'rel' => 'mask-icon',
  //       'color' => '#007ac9',
  //     ],
  //   ];
  //   $variables['#attached']['html_head'][] = [$safari, 'safari'];

  //   // Theme-color.
  //   $themecolor = [
  //     '#tag' => 'meta',
  //     '#attributes' => [
  //       'name' => 'theme-color',
  //       'content' => '#ffffff',
  //     ],
  //   ];
  //   $variables['#attached']['html_head'][] = [$themecolor, 'themecolor'];

  //   // Typekit
  //   $typekit = [
  //     '#tag' => 'link',
  //     '#attributes' => [
  //       'rel' => 'stylesheet',
  //       'href' => 'https://use.typekit.net/xte4arw.css'
  //     ],
  //   ];
  //   $variables['#attached']['html_head'][] = [$typekit, 'typekit'];
}
