<?php

/**
 * @file
 * Provides node processing functionality.
 */

/**
 * Implements template_preprocess_HOOK() for node.html.twig;.
 */
function gars_preprocess_node(&$variables) {
  $node = $variables['node'];
  $bundle = $node->getType();

  $node_name_clean = str_replace('_', '-', $bundle);
  $variables['name'] = $node_name_clean;
}
