<?php

/**
 * @file
 * Provides user processing functionality.
 */

/**
 * Implements hook_theme_suggestions_user_alter().
 *
 *   An array of alternate, more specific names for template files or theme
 *   functions.
 */
function gars_theme_suggestions_user_alter(&$suggestions, $vars, $hook) {
  $mode = $vars['elements']['#view_mode'];
  $suggestions[] = 'user__' . $mode;
}
