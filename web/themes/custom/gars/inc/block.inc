<?php

/**
 * @file
 * Provides block processing functionality.
 */

use Drupal\Component\Utility\Html;
use Drupal\block\Entity\Block;

/**
 * Implements hook_preprocess_HOOK() for block.html.twig.
 */
function gars_preprocess_block(&$variables) {
  // var_dump($variables['elements']['#id']);
  // Clean word "block" from plugin id and prepare a clean CSS name.
  $plugin_id = preg_replace('/[^A-Za-z\-]/', '', $variables['elements']['#id']);
  $class_name = strtolower(Html::cleanCssIdentifier($plugin_id));
  $class_name = rtrim($class_name, '-');

  // All blocks get block and block__title classes.
  $variables['attributes']['class'][] = 'block';
  $variables['title_attributes']['class'][] = 'block__title';

  // Give block also an identifying class.
  $variables['attributes']['class'][] = $class_name;
  $variables['title_attributes']['class'][] = $class_name . '__title';

  // Add region variable to the template.
  $variables['region'] = '';

  // Set region as modifier class.
  $region = Block::load($variables['elements']['#id']);
  if ($region instanceof Block) {
    $variables['attributes']['class'][] = $class_name . '--' . $region->getRegion();
    $variables['region'] = $region->getRegion();
  }

  // If system branding block.
  if ($variables['elements']['#plugin_id'] == 'system_branding_block') {
    // Add a language modifier class.
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $variables['attributes']['class'][] = $class_name . '--' . $language;

    // Add language variable to the template.
    $variables['language'] = $language;
  }
}
