const path = require('path')
const merge = require('webpack-merge')

// Plugins
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

// Base entries
const baseEntries = [
  { theme: `./themes/custom/gars/assets/index.js` } // Main theme entry
]

// Base configuration
const baseConfiguration = {
  context: path.resolve(__dirname, 'web/'),
  resolve: {
    alias: {
      Lib: path.resolve(__dirname, 'lib/')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      },
      // CSS
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          {
            loader: 'resolve-url-loader'
          },
          'postcss-loader'
        ]
      },
      // Sass
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              includePaths: [
                path.resolve(__dirname, 'lib')
              ]
            }
          }
        ]
      },
      {
        test: /\.(gif|png|jpe?g)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              publicPath: '/web/assets/images/',
              outputPath: 'images/'
            }
          },
          'image-webpack-loader'
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-sprite-loader',
            options: {
              extract: true
            }
          },
          'svgo-loader'
        ]
      }
    ]
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'web/assets/')
  },
  plugins: [
    // Clean dist folder before building
    new CleanWebpackPlugin(['web/assets/']),

    // Extract CSS to its own file
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),

    // Create SVG sprite
    new SpriteLoaderPlugin()
  ]
}

const exportArray = [...baseEntries.map((entry) => {
  return merge(baseConfiguration, {
    entry
  })
})]

module.exports = exportArray
